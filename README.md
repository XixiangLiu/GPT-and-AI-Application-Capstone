# GPT And AI Application Capstone

## Project Description
* This is a GPT and AI Application Capstone project.
* We try to develop an industry level GPT application.

## Team
* Leader: Xixiang Liu
* Members: Rui Wen, Xiaohua Li, Songquan Dong

## File Structure
* Group_Presentations: storing our team's weekly presentations.
* Resources: storing course resources.
* TimeLog.docx: recording each team member's work time per week.
